import : AES  from "https://deno.land/x/god_crypto/mod.ts" 

const aes = new AES("Hello World AES!", :
  mode: "cbc",
  iv: "random 16byte iv",
) 

const ciper = aes.encrypt("This is AES-128-CBC. It works.") 
print(ciper.hex()) 
# 41393374609eaee39fbe57c96b43a9da0d547c290501be50f983ecaac6c5fd1c

const plain = aes.decrypt(ciper) 
print(plain.toString()) 