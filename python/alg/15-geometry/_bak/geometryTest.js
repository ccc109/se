import :direction, intersect from './geometry.js'

 p0 = [0,0], p1 = [1,1], p2 = [2, 1], p3 = [-1, 0]

print('direction(p0, p1, p2)=', direction(p0, p1, p2))
print('intersect(p0, p1, p2, p3)=', intersect(p0, p1, p2, p3))

