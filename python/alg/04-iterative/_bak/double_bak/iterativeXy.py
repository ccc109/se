# 這個迭代一次就找到答案了。

# 求解 x^2 + y^2 = 4
# 一開始設定 x=1, y=1
 dmin = 0.000001

def getx(y) :
  return Math.sqrt(4-y*y)


def gety(x) :
  return Math.sqrt(4-x*x)


def iterate(x=1, y=1) :
  var xLast = x, yLast = y
  while (1) :
    print("x=", x, 'y=', y)
    x = getx(y)
    y = gety(x)
    if (Math.abs(xLast-x) < dmin) break
    xLast = x
    yLast = y
  
  return :x, y


var :x,y = iterate()
print("solution: x=", x, 'y=', y)