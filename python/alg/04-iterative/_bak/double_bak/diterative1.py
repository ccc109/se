''' x^2 - 2x + 1 = 0 改寫

y = x^2
2x-1 = y    => x=(y+1)/2
'''

def f(x) :
  return x*x


def g(y) :
  return (y+1)/2


def iterate(g, x) :
  print("x=", x) 
  for (var i=0  i<100000  i++) :
    if (Math.abs(x-g(x)) < 0.001)
      return x
    x = g(f(x))
    print("x=", x) 
  
  return x


# var x = iterate(g, 10) # 會發散
var x = iterate(g, 0.5) # 會收斂
print("x=", x, "g(f(x))=", g(f(x)))

''' 永遠收斂的案例
x = 0.8*sqrt(x^2) = 0.8x .....

def f(x) :
  return x*x


def g(y) :
  return 0.8*Math.sqrt(y)

'''
