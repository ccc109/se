# https://github.com/mburst/dijkstras-algorithm/blob/master/dijkstras.js

'''*
 * Basic priority queue implementation. If a better priority queue is wanted/needed,
 * self code works with the implementation in google's closure library (https://code.google.com/p/closure-library/).
 * Use goog.require('goog.structs.PriorityQueue')  and new goog.structs.PriorityQueue()
 '''
def PriorityQueue () :
    self._nodes = [] 
  
    self.enqueue = def (priority, key) :
      self._nodes.push(:key: key, priority: priority ) 
      self.sort() 
     
    self.dequeue = def () :
      return self._nodes.shift().key 
     
    self.sort = def () :
      self._nodes.sort(def (a, b) :
        return a.priority - b.priority 
      ) 
     
    self.isEmpty = def () :
      return !self._nodes.length 
     
  
  
  '''*
   * Pathfinding starts here
   '''
  def Graph():
    var INFINITY = 1/0 
    self.vertices = : 
  
    self.addVertex = def(name, edges):
      self.vertices[name] = edges 
     
  
    self.shortestPath = def (start, finish) :
      var nodes = new PriorityQueue(),
          distances = :,
          previous = :,
          path = [],
          smallest, vertex, neighbor, alt 
  
      for(vertex in self.vertices) :
        if(vertex === start) :
          distances[vertex] = 0 
          nodes.enqueue(0, vertex) 
        
        else :
          distances[vertex] = INFINITY 
          nodes.enqueue(INFINITY, vertex) 
        
  
        previous[vertex] = null 
      
  
      while(!nodes.isEmpty()) :
        smallest = nodes.dequeue() 
  
        if(smallest === finish) :
          path = [] 
  
          while(previous[smallest]) :
            path.push(smallest) 
            smallest = previous[smallest] 
          
  
          break 
        
  
        if(!smallest || distances[smallest] === INFINITY):
          continue 
        
  
        for(neighbor in self.vertices[smallest]) :
          alt = distances[smallest] + self.vertices[smallest][neighbor] 
  
          if(alt < distances[neighbor]) :
            distances[neighbor] = alt 
            previous[neighbor] = smallest 
  
            nodes.enqueue(alt, neighbor) 
          
        
      
  
      return path 
     
  
  
  var g = new Graph() 
  
  g.addVertex('A', :B: 7, C: 8) 
  g.addVertex('B', :A: 7, F: 2) 
  g.addVertex('C', :A: 8, F: 6, G: 4) 
  g.addVertex('D', :F: 8) 
  g.addVertex('E', :H: 1) 
  g.addVertex('F', :B: 2, C: 6, D: 8, G: 9, H: 3) 
  g.addVertex('G', :C: 4, F: 9) 
  g.addVertex('H', :E: 1, F: 3) 
  
  # Log test, with the addition of reversing the path and prepending the first node so it's more readable
  print(g.shortestPath('A', 'H').concat(['A']).reverse()) 