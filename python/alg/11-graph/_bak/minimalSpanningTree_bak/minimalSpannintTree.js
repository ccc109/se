# https://gist.github.com/n8agrin/3629426

var _ = require('underscore') 

var nodes = ["A", "B", "C", "D", "E", "F", "G"] 
var edges = [
    ["A", "B", 7], ["A", "D", 5],
    ["B", "C", 8], ["B", "D", 9], ["B", "E", 7],
    ["C", "E", 5],
    ["D", "E", 15], ["D", "F", 6],
    ["E", "F", 8], ["E", "G", 9],
    ["F", "G", 11]
] 


def kruskal(nodes, edges) :
    var mst = [] 
    var forest = _.map(nodes, def(node) : return [node]  ) 
    var sortedEdges = _.sortBy(edges, def(edge) : return -edge[2]  ) 
    while(forest.length > 1) :
        var edge = sortedEdges.pop() 
        var n1 = edge[0],
            n2 = edge[1] 

        var t1 = _.filter(forest, def(tree) :
            return _.include(tree, n1) 
        ) 
            
        var t2 = _.filter(forest, def(tree) :
            return _.include(tree, n2) 
        ) 

        if (t1 != t2) :
            forest = _.without(forest, t1[0], t2[0]) 
            forest.push(_.union(t1[0], t2[0])) 
            mst.push(edge) 
        
    
    return mst 


print(kruskal(nodes, edges)) 