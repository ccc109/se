# https://gist.github.com/benlesh/9128359

'''*
 * Binary Tree
 * (c) 2014 Ben Lesh <ben@benlesh.com>
 * MIT license
 '''

'''
 * A simple Binary Tree implementation in JavaScript
 '''


export def BinaryTree() :
    var self = self 
    var root 
    
    def traverse(value, fn) :
        var found = root,
            side, parent 
        while(found && found.value !== value) :
            parent = found 
            if(value > found.value) :
                side = 'right' 
                found = found.right     
             else :
                side = 'left' 
                found = found.left 
            
        
        return : found: found, parent: parent, side: side  
    
    
    self.add = def (value, item) :
        if(typeof value === 'undefined') :
            throw new Error('value cannot be undefined') 
        
        
        var node = new BinaryTreeNode(value, item) 
        if(!root) :
            root = node 
            return 
        
        
        var result = traverse(value) 
        if(!result.found) :
            result.parent[result.side] = node 
         else :
            throw new Error('two items of the same value added') 
        
     

    self.search = def(value) :
        var result = traverse(value) 
        return result.found ? result.found.item : null 
     
    
    self.contains = def(value) :
        return !!self.find(value)
     
    
    self.root = def ():
        return root 
     
    
    def BinaryTreeNode(value, item) :
        self.value = value 
        self.item = item 
        self.left = null 
        self.right = null 
    
 