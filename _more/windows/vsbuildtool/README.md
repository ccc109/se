# Microsoft C++ Build Tools

C 語言編譯器 cl

* C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Tools\MSVC\14.25.28610\bin\Hostx64\x64

```
PS C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Tools\MSVC\14.25.28610\bin\Hostx64\x64> dir cl.*


    目錄: C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\ 
    VC\Tools\MSVC\14.25.28610\bin\Hostx64\x64


Mode                LastWriteTime         Length Name
----                -------------         ------ ----
-a----      2020/5/18  下午 07:35         428968 cl.exe
-a----      2020/5/18  下午 07:35            409 cl.exe.config
```

## 下載

* https://visualstudio.microsoft.com/zh-hant/visual-cpp-build-tools/

## 用法

* [Walkthrough: Compiling a Native C++ Program on the Command Line](https://docs.microsoft.com/en-us/cpp/build/walkthrough-compiling-a-native-cpp-program-on-the-command-line?redirectedfrom=MSDN&view=vs-2019)


The Build Tools for Visual Studio installs only the command-line compilers, tools, and libraries you need to build C and C++ programs. It's perfect for build labs or classroom exercises and installs relatively quickly. To install only the command-line tools, look for Build Tools for Visual Studio on the Visual Studio Downloads page.


If you have installed Visual Studio 2017 or later on Windows 10, open the Start menu and choose All apps. Scroll down and open the Visual Studio folder (not the Visual Studio application). Choose Developer Command Prompt for VS to open the command prompt window.



