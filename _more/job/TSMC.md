# TSMC jobs

軟體開發工程師
Description
 
1. A.I. technology/application research, design and development.
2. Software engineering and scrum software development.
3. Initial, innovate, develop and introduce Intelligent Manufacturing Solutions.
4. System analysis, design, development and integration for cross organizations/systems projects.
 
Qualifications
 
1. Master degree or above and major in Computer Science, Information enginnering,Industry Engineering, Statistics or Mathematic related fields.
2. Strong technical skills in at least one of the following fields: C++,C#, JAVA, , Database or Systeme infrastructure architect.
3. With Optimization, scheduling or A.I. related experience and skill is better.
4. Good communication skills.
5. Work location : Hsinchu, Taichung or Tainan.
 
Primary Location: Taiwan
Job: Information Technology
Job Posting Date: Oct 27, 2020
Unposting Date: Ongoing

IT Software Developers
Description
 
Building world-class factory infrastructure at record speed and ramped to high-volume production is key to enable tsmc to scale worldwide. We are looking for a highly motivated engineer to join our global systems development team within supporting tsmc factories worldwide. The ideal candidate has a highly technical multi-discipline engineering skillset that can push the limits of complex system design at large scale.
 
1.Develop state of the art code
2.Continue to refactor existing applications
3.Contribute to write tests to ensure software quality
4.Apply software design principles to ensure software quality
5.Ensure sustainability and performance of software applications
6.Collaborate with colleagues in design and code reviews.
7.Willing to learn new IT technology
 
Qualifications
 
1.BS/MS degree or above and major in Computer Science, Information engineering, Industry Engineering, Statistics or Mathematic related fields, similar technical field of study or equivalent practical experience.
2.Good at any listed  programming languages : C++,C#, JAVA, Python, GO, JavaScript.
3.Familiar with software engineering methodologies: UP, XP or DevOps
4.Familiar with software engineering practices: CI, CD, DDD or TDD
5.Have foundation of OOP, design principles and design patterns
6.Experience managing container-based workloads, using Kubernetes or other orchestration software is a plus.
7.Good communication skills with proactive, good interpersonal and problem-solving capability.
8.With AI related experience is a plus.
9.Familiar with source code version control tools : Git
 
Primary Location: Taiwan
Job: Information Technology
Job Posting Date: Oct 27, 2020
Unposting Date: Ongoing

DevOps Manager / Engineer
Description
 
Building world-class factory infrastructure at record speed and ramped to high-volume production is key to enable tsmc to scale worldwide. We are looking for a highly motivated engineer to join our global DevOps team within supporting tsmc factories worldwide. The ideal candidate has a highly technical multi-discipline engineering skillset that can push the limits of complex system operation at large scale.

 

1.Develop state of the art code for operational efficiency
2.Re-architect the tech debt legacy applications
3.Contribute to the software DevOPs culture of the group
4.Develop and maintain relationships across groups with all the stakeholders in the system
5.Review and extend software architecture to ensures the scalability of the system
6.Deploy and operate system to production and monitor service health
7.Report and track issues for software release
8.Willing to learn new IT technology
 
Qualifications
 
1.Bachelor degree or above and major in Computer Science, Information engineering, Industry Engineering, Statistics or Mathematic related fields, similar technical field of study or equivalent practical experience.
2.Good at any listed  programming languages : C++,C#, JAVA, Python, GO, JavaScript.
3.Familiar with software engineering methodologies: UP, XP or DevOps
4.Familiar with software engineering practices: CI, CD, DDD or TDD
5.Have foundation of OOP, design principles and design patterns
6.Comfortable using the tools of the trade; debuggers, source control, profiling, unit test coverage.
7.Experience managing container-based workloads, using Kubernetes or other orchestration software
8.With experience with Scrum or Devops practice is a plus.
9.With AI related experience is a plus.
10.Familiar with source code version control tools : Git
 
Primary Location: Taiwan-Hsinchu
Job: Information Technology
Job Posting Date: Jul 9, 2019
Unposting Date: Ongoing

Cloud Platform Architect
Description
 
 

- We are seeking experienced cloud architect to join TSMC IT infrastructure team to build and operate IT advanced cloud platform to support world-class semiconductor foundry 

- This team is responsible to design, implement and optimize IT infrastructure toward software-defined computing, storage and networking with advanced cloud technologies.

- The successful candidate should have strong technical skills and management experience 

1.Responsible for one or multiple roles below:
- Leverage modern software engineering practices and principles to build safe and reliable platform services 
- Work closely with our application teams to ensure all capabilities align with actual application delivery needs and pain points
- Coach and mentor development teams on usage and adoption of our Continuous Delivery toolsets and overall infrastructure as code and automation best-practices
- Build and coach on technical capabilities to enable faster innovation, accelerate time-to-market for all of our consumer experiences, and deliver industry leading developer experiences
2. Continuous improvement tasks for operation excellence
3. IT new technology survey and adoption for productivity improvement
 

 

 
 
Qualifications
 
1.BS/MS degree or above and major in Computer Science, Information engineering, automation or electrical engineering or similar technical field with equivalent practical experience.
2.Strong project management, communications, and interpersonal skills for internal and external organizations 
3.Industry certifications such as Cisco CCNA/CCNP/CCDP/CCSP, VMware, RHCE, or Microsoft (MCSA, MCSE) are desired.
4.Prefer experiences at least two fields below:
- 5 year+ using WMWare cloud or similar VM technology to build and operate large scale cloud infrastructure.
- 3+ years in design/implement K8S/Docker clusters with software defined network, storage.
- 5+ years in implementing Software Defined Network, Software Define Storage include file system, object storage, and DB as service with scalable architecture.
- 5+ years in design and implementing automation and monitoring to efficiently operate cloud infra, services, and health.
5.Both deep experience on development and operation manager role.
 
Primary Location: Taiwan-Hsinchu
Job: Information Technology
Job Posting Date: Jul 9, 2019
Unposting Date: Ongoing