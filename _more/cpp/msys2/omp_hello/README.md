

```
user@DESKTOP-96FRN6B MINGW64 /d/ccc/ccc109a/se/cpp/msys2
$ mkdir omp_hello

user@DESKTOP-96FRN6B MINGW64 /d/ccc/ccc109a/se/cpp/msys2
$ cd omp_hello

user@DESKTOP-96FRN6B MINGW64 /d/ccc/ccc109a/se/cpp/msys2/omp_hello
$ wget https://computing.llnl.gov/tutorials/openMP/samples/C/omp_hello.c
--2020-07-23 11:09:21--  https://computing.llnl.gov/tutorials/openMP/samples/C/omp_hello.c
▒▒▒b▒d▒▒D▒▒ computing.llnl.gov (computing.llnl.gov)... 198.128.253.1
▒▒▒b▒s▒▒ computing.llnl.gov (computing.llnl.gov)|198.128.253.1|:443... ▒s▒W▒F▒C
▒w▒e▒X HTTP ▒n▒D▒A▒▒▒b▒▒▒Ԧ^▒▒... 200 OK
▒▒▒▒: 1169 (1.1K) [text/plain]
Saving to: 'omp_hello.c'

     0K .                                                     100%  812K=0.001s

2020-07-23 11:09:23 (812 KB/s) - 'omp_hello.c' saved [1169/1169]


user@DESKTOP-96FRN6B MINGW64 /d/ccc/ccc109a/se/cpp/msys2/omp_hello
$ ls
omp_hello.c

user@DESKTOP-96FRN6B MINGW64 /d/ccc/ccc109a/se/cpp/msys2/omp_hello
$ gcc -fopenmp omp_hello.c -o omp_hello

user@DESKTOP-96FRN6B MINGW64 /d/ccc/ccc109a/se/cpp/msys2/omp_hello
$ export OMP_NUM_THREADS=8

user@DESKTOP-96FRN6B MINGW64 /d/ccc/ccc109a/se/cpp/msys2/omp_hello
$ ./omp_hello
Hello World from thread = 2
Hello World from thread = 5
Hello World from thread = 1
Hello World from thread = 6
Hello World from thread = 4
Hello World from thread = 3
Hello World from thread = 0
Number of threads = 8
Hello World from thread = 7

user@DESKTOP-96FRN6B MINGW64 /d/ccc/ccc109a/se/cpp/msys2/omp_hello
$ export OMP_NUM_THREADS=4

user@DESKTOP-96FRN6B MINGW64 /d/ccc/ccc109a/se/cpp/msys2/omp_hello
$ ./omp_hello
Hello World from thread = 1
Hello World from thread = 2
Hello World from thread = 0
Number of threads = 4
Hello World from thread = 3

user@DESKTOP-96FRN6B MINGW64 /d/ccc/ccc109a/se/cpp/msys2/omp_hello

```