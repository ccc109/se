# hello googletest


## ubuntu Linux Run

* Getting started with Google Test (GTest) on Ubuntu

用 g++

```
root@localhost:/home/guest/code/se/cpp/test/gtest/hello1# g++ tests.cpp -lgtest -lgtest_main -lpthread -o test 
s
root@localhost:/home/guest/code/se/cpp/test/gtest/hello1# ./tests
[==========] Running 2 tests from 1 test case.
[----------] Global test environment set-up.
[----------] 2 tests from SquareRootTest
[ RUN      ] SquareRootTest.PositiveNos
[       OK ] SquareRootTest.PositiveNos (0 ms)
[ RUN      ] SquareRootTest.NegativeNos
[       OK ] SquareRootTest.NegativeNos (0 ms)
[----------] 2 tests from SquareRootTest (0 ms total)

[----------] Global test environment tear-down
[==========] 2 tests from 1 test case ran. (0 ms total)
[  PASSED  ] 2 tests.
```


用 CMake

```
guest@localhost:~/code/se/cpp/test/googletest/hello$ cmake CMakeLists.txt
-- The C compiler identification is GNU 7.5.0
-- The CXX compiler identification is GNU 7.5.0
-- Check for working C compiler: /usr/bin/cc
-- Check for working C compiler: /usr/bin/cc -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Detecting C compile features
-- Detecting C compile features - done
-- Check for working CXX compiler: /usr/bin/c++
-- Check for working CXX compiler: /usr/bin/c++ -- works
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Found GTest: /usr/lib/libgtest.a  
-- Configuring done
-- Generating done
-- Build files have been written to: /home/guest/code/se/cpp/test/googletest/hello
guest@localhost:~/code/se/cpp/test/googletest/hello$ make
Scanning dependencies of target runTests
[ 50%] Building CXX object CMakeFiles/runTests.dir/tests.cpp.o
[100%] Linking CXX executable runTests
[100%] Built target runTests
guest@localhost:~/code/se/cpp/test/googletest/hello$ ./runTests
[==========] Running 2 tests from 1 test case.
[----------] Global test environment set-up.
[----------] 2 tests from SquareRootTest
[ RUN      ] SquareRootTest.PositiveNos
[       OK ] SquareRootTest.PositiveNos (0 ms)
[ RUN      ] SquareRootTest.NegativeNos
[       OK ] SquareRootTest.NegativeNos (2 ms)
[----------] 2 tests from SquareRootTest (2 ms total)

[----------] Global test environment tear-down
[==========] 2 tests from 1 test case ran. (2 ms total)
[  PASSED  ] 2 tests.
```



## msys2 install

```
$ pacman -S mingw-w64-x86_64-gtest
```

## run

```
user@DESKTOP-96FRN6B MINGW64 /d/ccc/ccc109a/se/cpp/test/googletest/hello1
$ g++ tests.cpp -lgtest -lgtest_main -o tests.exe

user@DESKTOP-96FRN6B MINGW64 /d/ccc/ccc109a/se/cpp/test/googletest/hello1
$ ./tests
[==========] Running 2 tests from 1 test case.
[----------] Global test environment set-up.
[----------] 2 tests from SquareRootTest
[ RUN      ] SquareRootTest.PositiveNos
[       OK ] SquareRootTest.PositiveNos (0 ms)
[ RUN      ] SquareRootTest.NegativeNos
[       OK ] SquareRootTest.NegativeNos (0 ms)
[----------] 2 tests from SquareRootTest (0 ms total)

[----------] Global test environment tear-down
[==========] 2 tests from 1 test case ran. (0 ms total)
[  PASSED  ] 2 tests.

```
