# Gtest


# gtest linux


* https://blog.csdn.net/weixin_34192732/article/details/94463041

```
root@localhost:/home/guest/code/se/cpp/test/gtest/hello0# apt install libgtest-dev
Reading package lists... Done
Building dependency tree
Reading state information... Done
libgtest-dev is already the newest version (1.8.0-6).
0 upgraded, 0 newly installed, 0 to remove and 341 not upgraded.
root@localhost:/home/guest/code/se/cpp/test/gtest/hello0# ls /usr/src/gtest   
build-aux       cmake_install.cmake  docs             make         scripts
CHANGES         CMakeLists.txt       include          Makefile     src    
cmake           codegear             libgtest.a       Makefile.am  test 
CMakeCache.txt  configure.ac         libgtest_main.a  README.md    xcode
CMakeFiles      CONTRIBUTORS         m4               samples
```

然後


```
guest@localhost:~/code/se/cpp/test/gtest/hello0$ g++ ./test_example.cpp -lgtest -lgtest_main -lpthread -o test 
_example
guest@localhost:~/code/se/cpp/test/gtest/hello0$ ./test_example
Running main() from gtest_main.cc
[==========] Running 1 test from 1 test case.
[----------] Global test environment set-up.
[----------] 1 test from Practice
[ RUN      ] Practice.First
[       OK ] Practice.First (0 ms)
[----------] 1 test from Practice (0 ms total)

[----------] Global test environment tear-down
[==========] 1 test from 1 test case ran. (0 ms total)
[  PASSED  ] 1 test.
```

## msys2

參考 -- https://mikoto2000.blogspot.com/2018/08/msys2-google-test.html

```
user@DESKTOP-96FRN6B MINGW64 /d/ccc/ccc109a/se/cpp/test/googletest

$ pacman -S mingw-w64-x86_64-toolchain mingw-w64-x86_64-gtest

$ ls /mingw64/lib/libgtest*.*
/mingw64/lib/libgtest.dll.a  /mingw64/lib/libgtest_main.dll.a

$ cd hello0

$ g++ ./test_example.cpp -lgtest -lgtest_main -o test_example.exe

$ ./test_example
Running main() from gtest_main.cc
[==========] Running 1 test from 1 test case.
[----------] Global test environment set-up.
[----------] 1 test from Practice
[ RUN      ] Practice.First
[       OK ] Practice.First (0 ms)
[----------] 1 test from Practice (0 ms total)

[----------] Global test environment tear-down
[==========] 1 test from 1 test case ran. (0 ms total)
[  PASSED  ] 1 test.

```