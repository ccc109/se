import 'package:crypto/crypto.dart';
import 'dart:convert'; // for the utf8.encode method
import 'dart:math';

hash(text) {
  var bytes = utf8.encode(text); // data being hashed
  var digest = sha1.convert(bytes);
  return '$digest';
}

final _random = new Random();

mining(record) {
  // for (var nonce=0; nonce<1000000000000; nonce++) {
  while (true) {
    var nonce = _random.nextInt(100000000); // Math.floor(Math.random()*100000000)
    record['nonce'] = nonce;
    var h = hash('$record');
    if (h.startsWith('00000')) return { 'nonce': nonce, 'hash': h };
  }
}

main() {
  var record = {
    'nonce': 0,
    'data': 'john => mary : \$2.7; george => john : \$1.3',
  };
  print(mining(record));
}
