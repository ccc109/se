sort(a) { // List sort(List a)
  for (var i = 0; i < a.length; i++) {
    for (var j=0; j<i; j++) {
      if (a[j] > a[i]) {
        var t = a[i];
        a[i] = a[j];
        a[j] = t;
      }
    }
  }
  return a;
}

main() {
  print('sort([3, 8, 2, 1, 5]=${sort([3,8,2,1,5])}');
}

