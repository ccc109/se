// Although Effective Dart recommends type annotations for public APIs, the function still works if you omit the types:
var _nobleGases = {
  2: 'helium',
  10: 'neon',
  18: 'argon',
};

isNoble(atomicNumber) {
  return _nobleGases[atomicNumber] != null;
}

main() {
  
}
