class Television {
  /// _Deprecated: Use [turnOn] instead._
  @deprecated
  void activate() {
    turnOn();
  }

  /// Turns the TV's power on.
  void turnOn() {
    print('turnOn()');
  }
}

main() {
  var tv = Television();
  tv.turnOn();
  tv.activate(); // lint 會用刪除線標示這一行
}
