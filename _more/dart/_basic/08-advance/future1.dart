// Adding the async keyword to a function makes it return a Future

Future<String> lookUpVersion() async => '1.0.0';

Future checkVersion() async {
  var version = await lookUpVersion();
  print('checkVersion: version=${version}');
  // Do something with version
  return version;
}

Future main() async {
  var v = await checkVersion();
  print('v=$v');
  print('In main: version is ${await lookUpVersion()}');
}
