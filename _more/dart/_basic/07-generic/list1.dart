main() {
  var names = List<String>();
  names.addAll(['Seth', 'Kathy', 'Lars']);
  names.add(42); // Error
}
/* 結果
PS D:\ccc\ccc109a\se\dart\_basic\07-generic> dart list1.dart 
list1.dart:4:13: Error: The argument type 'int' can't be assigned to the parameter type 'String'.
  names.add(42); // Error
            ^
*/