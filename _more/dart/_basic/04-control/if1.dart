  // Slightly longer version uses ?: operator.
  String playerName1(String name) => name != null ? name : 'Guest';

  // Very long version uses if-else statement.
  String playerName2(String name) {
    if (name != null) {
      return name;
    } else {
      return 'Guest';
    }
  }

main() {
  print('playerName1()=${playerName1(null)}');
  print('playerName2()=${playerName2(null)}');
}