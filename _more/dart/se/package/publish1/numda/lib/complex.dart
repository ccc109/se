import 'dart:math' as math;
import 'package:sprintf/sprintf.dart';

class Complex {
  double a,b;

  Complex(double this.a,double this.b);

  conj() { return Complex(a, -b); }

  Complex operator +(Complex o) {
    return Complex(a+o.a, b+o.b);
  }

  Complex operator -(Complex o) {
    return Complex(a-o.a, b-o.b);
  }

  Complex operator *(o) {
    var c=o.a, d=o.b;
    return Complex(a*c-b*d, a*d+b*c);
  }
  
  Complex operator /(o) {
    var c=o.a, d=o.b;
    return Complex((a*c+b*d)/(c*c+d*d), (b*c-a*d)/(c*c+d*d));
  }

  bool operator ==(o) {
    return a==o.a && b==o.b;
  }

  log() {
    var r=a*a+b*b;
    var w = math.log(r)/2;
    var x = math.acos(a/math.sqrt(r));
    return Complex(w, x);
  }
  
  exp() {
    var r=math.exp(a);
    return Complex(r*math.cos(b), r*math.sin(b));
  }

  trunc(int prec) {
    return Complex(double.parse(a.toStringAsFixed(prec)), double.parse(b.toStringAsFixed(prec)));
  }

  str(int prec) {
    var c = this.trunc(prec);
    return c.toString();
  }

  toString() {
    var op = (b>=0)?"+":"-";
    return '$a$op${b.abs()}i';
  }

  static parse(String s) {
    try {
      var regexp = RegExp(r'^([^+-]*)(([+-]([\d.]+)?)i)?$');
      var match = regexp.firstMatch(s);
      // print('match.groupCount=${match.groupCount}');
      // print('match.group(1)=${match.group(1)}');
      // print('match.group(3)=${match.group(3)}');
      double a = double.parse(match.group(1));
      double b = (match.groupCount > 3 && match.group(3) != null)?double.parse(match.group(3)):0.0;
      // print('a=${a}');
      // print('b=${b}');
      return Complex(a, b);
    } catch (e) {
      var errMsg = "Complex.parse: format error! It should be be a+bi";
      throw FormatException(errMsg);
    }
  }

  static list(String s) {
    List<Complex> r = [];
    List<String> tokens = s.split(",");
    for (var token in tokens) {
      r.add(Complex.parse(token));
    }
    return r;
  }
}
