# dart in browser

成功，在 http://127.0.0.1:8080/ 看到 Your Dart app is running.

## 操作

* https://dart.dev/tutorials/web/get-started

```
$ pub global activate webdev
$ pub global activate stagehand
```


Warning: Pub installs executables into C:\Users\user\AppData\Local\Pub\Cache\hosted\pub.dartlang.org\pub_cache-0.2.3\bin, which is not on your path.You can fix that by adding that directory to your system's "Path" environment variable.

加入 C:\Users\user\AppData\Local\Pub\Cache\hosted\pub.dartlang.org\pub_cache-0.2.3\bin 到 path
```

## 

```
Windows PowerShell
Copyright (C) Microsoft Corporation. 著作權所有，並保留一切權利。

請嘗試新的跨平台 PowerShell https://aka.ms/pscore6

PS D:\ccc\ccc109a\se\dart\_more\browser> cd .\quickstart\
PS D:\ccc\ccc109a\se\dart\_more\browser\quickstart> stagehand
Welcome to Stagehand! We collect anonymous usage statistics and crash reports in
order to improve the tool (http://goo.gl/6wsncI). Would you like to opt-in to
additional analytics to help us improve Stagehand [y/yes/no]?
y

Stagehand will generate the given application type into the current directory.

usage: stagehand <generator-name>
    --[no-]analytics    Opt out of anonymous usage and crash reporting.
    --version           Display the version for stagehand.
    --author            The author name to use for file headers.
                        (defaults to "<your name>")

Available generators:
  console-full   - A command-line application sample.
  console-simple - A simple command-line application.
  package-simple - A starting point for Dart libraries or applications.   
  server-shelf   - A web server built using the shelf package.
  web-angular    - A web app with material design components.
  web-simple     - A web app that uses only core Dart libraries.
  web-stagexl    - A starting point for 2D animation and games.
PS D:\ccc\ccc109a\se\dart\_more\browser\quickstart> stagehand web-simple  
Creating web-simple application `quickstart`:
  D:\ccc\ccc109a\se\dart\_more\browser\quickstart\.gitignore
  D:\ccc\ccc109a\se\dart\_more\browser\quickstart\CHANGELOG.md
  D:\ccc\ccc109a\se\dart\_more\browser\quickstart\README.md
  D:\ccc\ccc109a\se\dart\_more\browser\quickstart\analysis_options.yaml   
  D:\ccc\ccc109a\se\dart\_more\browser\quickstart\pubspec.yaml
  D:\ccc\ccc109a\se\dart\_more\browser\quickstart\web/favicon.ico
  D:\ccc\ccc109a\se\dart\_more\browser\quickstart\web/index.html
  D:\ccc\ccc109a\se\dart\_more\browser\quickstart\web/main.dart
  D:\ccc\ccc109a\se\dart\_more\browser\quickstart\web/styles.css
9 files written.

--> to provision required packages, run 'pub get'
PS D:\ccc\ccc109a\se\dart\_more\browser\quickstart> pub get
Resolving dependencies...
Got dependencies!
PS D:\ccc\ccc109a\se\dart\_more\browser\quickstart> webdev serve
[SEVERE] Failed to precompile build_runner:build_runner:
Unhandled exception:
Bad state: Unable to start build daemon.
#0      _handleDaemonStartup (package:build_daemon/client.dart:82:5)
<asynchronous suspension>
#1      BuildDaemonClient.connect (package:build_daemon/client.dart:183:11)
<asynchronous suspension>
#2      connectClient (package:webdev/src/daemon_client.dart:17:23)
#3      _startBuildDaemon (package:webdev/src/serve/dev_workflow.dart:26:18)
#4      DevWorkflow.start (package:webdev/src/serve/dev_workflow.dart:191:24)
#5      ServeCommand.run (package:webdev/src/command/serve_command.dart:101:27)
<asynchronous suspension>
#6      CommandRunner.runCommand (package:args/command_runner.dart:197:27)#7      _CommandRunner.runCommand (package:webdev/src/webdev_command_runner.dart:38:24)
#8      CommandRunner.run.<anonymous closure> (package:args/command_runner.dart:112:25)
#9      new Future.sync (dart:async/future.dart:224:31)
#10     CommandRunner.run (package:args/command_runner.dart:112:14)       
#11     run (package:webdev/src/webdev_command_runner.dart:19:56)
#12     main (file:///C:/Users/user/AppData/Local/Pub/Cache/hosted/pub.dartlang.org/webdev-2.5.9/bin/webdev.dart:17:22)
#13     _startIsolate.<anonymous closure> (dart:isolate-patch/isolate_patch.dart:299:32)
#14     _RawReceivePortImpl._handleMessage (dart:isolate-patch/isolate_patch.dart:168:12)
[SEVERE] Failed to precompile build_runner:build_runner:
Unhandled exception:
Bad state: Unable to start build daemon.
#0      _handleDaemonStartup (package:build_daemon/client.dart:82:5)
<asynchronous suspension>
#1      BuildDaemonClient.connect (package:build_daemon/client.dart:183:11)
<asynchronous suspension>
#2      connectClient (package:webdev/src/daemon_client.dart:17:23)       
#3      _startBuildDaemon (package:webdev/src/serve/dev_workflow.dart:26:18)
#4      DevWorkflow.start (package:webdev/src/serve/dev_workflow.dart:191:24)
#5      ServeCommand.run (package:webdev/src/command/serve_command.dart:10r.dart:38:24)
#8      CommandRunner.run.<anonymous closure> (package:args/command_runner.dart:112:25)
#9      new Future.sync (dart:async/future.dart:224:31)
#10     CommandRunner.run (package:args/command_runner.dart:112:14)       
#11     run (package:webdev/src/webdev_command_runner.dart:19:56)
#12     main (file:///C:/Users/user/AppData/Local/Pub/Cache/hosted/pub.dartlang.org/pub_cache-0.2.3/hosted/pub.flutter-io.cn/webdev-2.5.9/bin/webdev.dart:17:22)
#13     _startIsolate.<anonymous closure> (dart:isolate-patch/isolate_patch.dart:299:32)
#14     _RawReceivePortImpl._handleMessage (dart:isolate-patch/isolate_patch.dart:168:12)
PS D:\ccc\ccc109a\se\dart\_more\browser\quickstart>
PS D:\ccc\ccc109a\se\dart\_more\browser\quickstart>
PS D:\ccc\ccc109a\se\dart\_more\browser\quickstart>
PS D:\ccc\ccc109a\se\dart\_more\browser\quickstart> webdev serve
[INFO] Building new asset graph completed, took 8.4s
[INFO] Checking for unexpected pre-existing outputs. completed, took 12ms
[INFO] Serving `web` on http://127.0.0.1:8080
[INFO] Running build completed, took 7.5s
[INFO] Caching finalized dependency graph completed, took 582ms
[INFO] Succeeded after 8.2s with 11 outputs (1318 actions)
[INFO] ------------------------------------------------------------------ 

```