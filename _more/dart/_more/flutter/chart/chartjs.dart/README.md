來源 -- https://github.com/google/chartjs.dart

## 執行結果

```
PS D:\ccc\ccc109a\se\dart\_more\flutter\chart\chartjs.dart> pub global run webdev serve example
[INFO] Building new asset graph completed, took 14.1s
[INFO] Checking for unexpected pre-existing outputs. completed, took 5ms
[INFO] Serving `example` on http://127.0.0.1:8080
[INFO] Running build completed, took 12.4s
[INFO] Caching finalized dependency graph completed, took 462ms
[INFO] Succeeded after 12.9s with 26 outputs (1344 actions)
[INFO] ------------------------------------------------------------------------------------------------------ 
```

然後在 http://127.0.0.1:8080 可以看到網頁！

## 原始說明

A generated Dart API for [Chart.js](https://www.chartjs.org/)
using [pkg/js](https://pub.dev/packages/js) and
[dart_js_facade_gen](https://github.com/dart-lang/js_facade_gen).

This project will be *lightly* maintained by the original author.
Contributions are welcome.

To understand usage, see the
[example](https://github.com/google/chartjs.dart/tree/master/example)
source code and the
[corresponding output](https://google.github.io/chartjs.dart/).

## Disclaimer

This is not an official Google product.


## CCC 使用

```
PS D:\ccc\ccc109a\se\dart\_more\flutter\chart> git clone https://github.com/google/chartjs.dart.git
Cloning into 'chartjs.dart'...
remote: Enumerating objects: 36, done.
remote: Counting objects: 100% (36/36), done.
remote: Compressing objects: 100% (29/29), done.
Receiving objects: 100% (315/315), 537.03 KiB | 409.00 KiB/s, done.
remote: Total 315 (delta 12), reused 20 (delta 6), pack-reused 279
Resolving deltas: 100% (112/112), done.
PS D:\ccc\ccc109a\se\dart\_more\flutter\chart> cd .\chartjs.dart\
PS D:\ccc\ccc109a\se\dart\_more\flutter\chart\chartjs.dart> pub global run webdev serve example
No active package webdev.
PS D:\ccc\ccc109a\se\dart\_more\flutter\chart\chartjs.dart> pub global activate webdev
Resolving dependencies...
+ args 1.6.0
+ async 2.4.2
+ browser_launcher 0.1.7
+ build_daemon 2.1.4
+ built_collection 4.3.2
+ built_value 7.1.0
+ charcode 1.1.3
+ checked_yaml 1.0.2
+ collection 1.14.13 (1.15.0-nullsafety available)
+ convert 2.1.1
+ crypto 2.1.5
+ devtools 0.2.5 (0.8.0+2 available)
+ devtools_server 0.2.5 (0.8.0+2 available)
+ devtools_shared 0.2.5 (0.8.0+2 available)
+ dwds 4.0.1 (5.1.0 available)
+ file 5.2.1
+ fixnum 0.10.11
+ http 0.12.2
+ http_multi_server 2.2.0
+ http_parser 3.1.4
+ intl 0.16.1
+ io 0.3.4
+ json_annotation 3.0.1
+ logging 0.11.4
+ matcher 0.12.9
+ meta 1.2.2 (1.3.0-nullsafety available)
+ mime 0.9.6+3
+ package_config 1.9.3
+ path 1.7.0
+ pedantic 1.9.2
+ pool 1.4.0
+ pub_semver 1.4.4
+ pubspec_parse 0.1.5
+ quiver 2.1.3
+ shelf 0.7.7
+ shelf_packages_handler 2.0.0
+ shelf_proxy 0.1.0+7
+ shelf_static 0.2.8
+ shelf_web_socket 0.2.3
+ source_maps 0.10.9
+ source_span 1.7.0
+ sse 3.5.0
+ stack_trace 1.9.5
+ stream_channel 2.0.0
+ stream_transform 1.2.0
+ string_scanner 1.0.5
+ term_glyph 1.1.0
+ typed_data 1.2.0 (1.3.0-nullsafety available)
+ usage 3.4.2
+ uuid 2.2.0
+ vm_service 4.1.0
+ watcher 0.9.7+15
+ web_socket_channel 1.1.0
+ webdev 2.5.9
+ webkit_inspection_protocol 0.7.3
+ yaml 2.2.1
Downloading webdev 2.5.9...
Downloading sse 3.5.0...
Downloading shelf_static 0.2.8...
Downloading dwds 4.0.1...
Downloading webkit_inspection_protocol 0.7.3...
Downloading vm_service 4.1.0...
Downloading shelf_proxy 0.1.0+7...
Downloading browser_launcher 0.1.7...
Downloading file 5.2.1...
Downloading shelf_packages_handler 2.0.0...
Downloading http 0.12.2...
Downloading uuid 2.2.0...
Downloading devtools_server 0.2.5...
Downloading devtools_shared 0.2.5...
Downloading intl 0.16.1...
Downloading usage 3.4.2...
Downloading devtools 0.2.5...
Precompiling executables...
Precompiled webdev:webdev.
Installed executable webdev.
Warning: Pub installs executables into C:\Users\user\AppData\Local\Pub\Cache\bin, which is not on your path.  
You can fix that by adding that directory to your system's "Path" environment variable.
A web search for "configure windows path" will show you how.
Activated webdev 2.5.9.

```