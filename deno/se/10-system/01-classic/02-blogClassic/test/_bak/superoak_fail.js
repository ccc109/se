import { app } from '../src/app.js'
import { superoak } from "https://deno.land/x/superoak@2.1.0/mod.ts";
import { expect } from "https://deno.land/x/expect@v0.2.1/mod.ts";


Deno.test("/", async () => {
  var request = await superoak(app)
  await request.get("/").expect(200).expect(/<title>Posts<\/title>/).expect(/<p>You have <strong>0<\/strong> posts!<\/p>/)
});

Deno.test("/post/new", async () => {
  var request = await superoak(app)
  await request.get("/post/new").expect(200).expect(/Create a new post/)
});

// superdeno 和 superoak 似乎都不支援 post，這個測試沒失敗卻也沒進入！
Deno.test("/post", async () => {
  var request = await superoak(app)
  await request.post("/post").send({title: 'Title', body: 'Contents'})
})

Deno.test("/", async () => {
  var request = await superoak(app)
  var res = await request.get("/")
  console.log('res.text=', res.text)
});
/*
Deno.test("/post", async () => {
  var request = await superoak(app)
  await request.get('/post/0').expect(200)
})
*/


/*
const server = app.listen();
const request = require('supertest').agent(server);

describe('Blog', function() {
  after(function() {
    server.close();
  });

  describe('GET /', function() {
    it('should see title "Posts"', function(done) {
      request
      .get('/')
      .expect(200, function(err, res) {
        if (err) return done(err);
        // res.should.be.html;
        // res.text.should.include('<title>Posts</title>');
        ok(res.text.indexOf('<title>Posts</title>') >= 0)
        done();
      });
    });
    it('should see 0 post', function(done) {
      request
      .get('/')
      .expect(200, function(err, res) {
        if (err) return done(err);

        // res.should.be.html;
        ok(res.text.indexOf('<p>You have <strong>0</strong> posts!</p>') >= 0)
        // res.text.should.include('<p>You have <strong>0</strong> posts!</p>');
        done();
      });
    });
  });

  describe('POST /post/new', function() {
    it('should create post and redirect to /', function(done) {
      request
      .post('/post')
      .send({title: 'Title', body: 'Contents'})
      .end(function(err, res) {
        if (err) return done(err);

        // res.header.location.should.be.equal('/');
        ok(res.headers.location == '/')
        done();
      });
    });
  });

  describe('GET /post/0', function() {
    it('should see post', function(done) {
      request
      .get('/post/0')
      .expect(200, function(err, res) {
        if (err) return done(err);

        // res.should.be.html;
        // console.log('headers=', res.header)
        ok(res.header['content-type'].indexOf('html') >= 0)
        ok(res.text.indexOf('<h1>Title</h1>') >= 0)
        // res.text.should.include('<h1>Title</h1>');
        ok(res.text.indexOf('<p>Contents</p>') >= 0)
        // res.text.should.include('<p>Contents</p>');
        done();
      });
    });
  });
});
*/