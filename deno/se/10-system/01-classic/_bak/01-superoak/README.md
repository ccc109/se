# 練習 -- koa server 測試入門

1. 請執行 deno run -A main.js 後，查看 http://127.0.0.1:8000/
2. 請執行 deno test -A test.js 進行測試。

```
PS D:\ccc\ccc109a\se\deno\se\10-system\01-classic\01-superoak> deno test -A test.js
Check file:///D:/ccc/ccc109a/se/deno/se/10-system/01-classic/01-superoak/.deno.test.ts
running 1 tests
test /=>Hello World! ... res= Response {
  req: Test {
    _query: [],
    method: "GET",
    url: "http://127.0.0.1:18779/",
    header: {},
    _header: {},
    _callbacks: { $end: [ [Function] ], $abort: [ [Function] ] },
    _maxRedirects: 0,
    app: Server { close: [Function], listener: { addr: [Object] } },
    _endCalled: true,
    _callback: [AsyncFunction],
    xhr: XMLHttpRequestSham {
      id: "3",
      origin: "http://127.0.0.1:18779",
      onreadystatechange: [Function],
      readyState: 4,
      responseText: "Hello World!",
      responseType: "text",
      response: "Hello World!",
      status: 200,
      statusCode: 200,
      statusText: "OK",
      aborted: false,
      options: {
        requestHeaders: [Object],
        method: "GET",
        url: "http://127.0.0.1:18779/",
        username: undefined,
        password: undefined,
        requestBody: null
      },
      controller: AbortController {},
      getAllResponseHeaders: [Function],
      getResponseHeader: [Function]
    },
    _fullfilledPromise: Promise { [Circular] }
  },
  xhr: XMLHttpRequestSham {
    id: "3",
    origin: "http://127.0.0.1:18779",
    onreadystatechange: [Function],
    readyState: 4,
    responseText: "Hello World!",
    responseType: "text",
    response: "Hello World!",
    status: 200,
    statusCode: 200,
    statusText: "OK",
    aborted: false,
    options: {
      requestHeaders: {},
      method: "GET",
      url: "http://127.0.0.1:18779/",
      username: undefined,
      password: undefined,
      requestBody: null
    },
    controller: AbortController {},
    getAllResponseHeaders: [Function],
    getResponseHeader: [Function]
  },
  text: "Hello World!",
  statusText: "OK",
  statusCode: 200,
  status: 200,
  statusType: 2,
  info: false,
  ok: true,
  redirect: false,
  clientError: false,
  serverError: false,
  error: false,
  created: false,
  accepted: false,
  noContent: false,
  badRequest: false,
  unauthorized: false,
  notAcceptable: false,
  forbidden: false,
  notFound: false,
  unprocessableEntity: false,
  headers: { content-length: "12", content-type: "text/plain; charset=utf-8" },
  header: { content-length: "12", content-type: "text/plain; charset=utf-8" },
  type: "text/plain",
  charset: "utf-8",
  links: {},
  body: null
}
ok (1562ms)

test result: ok. 1 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out (1649ms)
```
