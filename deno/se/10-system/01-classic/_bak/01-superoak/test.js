import { app } from './app.js'
import { superoak } from "https://deno.land/x/superoak@2.1.0/mod.ts";
import { expect } from "https://deno.land/x/expect@v0.2.1/mod.ts";

Deno.test("/=>Hello World!", async () => {
  var request = await superoak(app)
  await request.get("/").expect("Hello World!");

  request = await superoak(app)
  await request.get("/").expect(200).expect("Hello World!");
  
  request = await superoak(app)
  let res = await request.get("/")
  console.log('res=', res)
  expect(res.type).toBe('text/plain')
  expect(res.status).toBe(200)
  expect(res.text).toBe("Hello World!")
});
