const expect = require('se6').expect
const mm6 = require('../src')
const {G} = mm6
// const Vector = mm6.Vector

describe('mm6:geometry test', function() {
  it('geometry test', function() {
    let p0 = [0,0], p1 = [1,1], p2 = [2, 1], p3 = [-1, 0]
    expect(G.direction(p0, p1, p2)).to.equal(1)
    expect(G.intersect(p0, p1, p2, p3)).to.equal(true)
  })
  /* convex = [ 
    [ -1, -1, angle: -9999 ],
    [ 1, 0, angle: 0.4472135954999579 ],
    [ 1, 1, angle: 0.7071067811865475 ],
    [ 0, 1, angle: 0.8944271909999159 ]
  ] */
  it('convex test', function() {
    var points = [[0,0],[1,0],[1,1],[0,1],[.5,.5],[-1,-1]];
    var convex = G.convexHall(points)
    // console.log('convex=\n', convex)
    expect(convex.length).to.equal(4)
  })
})



